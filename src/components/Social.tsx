import { Box, Heading, Link, Progress, Text } from '@chakra-ui/react'
import { BsGithub, BsYoutube, BsDiscord } from 'react-icons/bs'

export const Social = () => {
	return (
		<Box bg={'blackAlpha.400'} mt={'2'} p={'4'} borderRadius={'20'}>
			<Heading
				display={'flex'}
				fontSize={'2xl'}
				fontWeight={'normal'}
				justifyContent={'center'}
			>
				Social
			</Heading>
			<Progress size={'xs'} mt={'2'} colorScheme={'red'} isIndeterminate />
			<Box
				mx={'auto'}
				display={'flex'}
				justifyContent={'center'}
				mt={'4'}
				fontSize={'2xl'}
			>
				<Link href='https://github.com/Z1xonu4a'>
					<BsGithub className={'ico'} />
					<Text>GitHub</Text>
				</Link>
				<Link href='https://discord.gg/TGemTBP9vQ' pl={'4'}>
					<BsDiscord className={'ico'} />
					<Text>Discord</Text>
				</Link>
				<Link href='https://www.youtube.com/@z1xonu4a' pl={'4'}>
					<BsYoutube className={'ico'} />
					<Text>Youtube</Text>
				</Link>
			</Box>
		</Box>
	)
}
