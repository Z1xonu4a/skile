import { Avatar, Box, Collapse, Flex, useDisclosure } from '@chakra-ui/react'

import img from '../image/avatar.jpg'
import { Name } from './Name'

export const Avatars = () => {
	const { isOpen, onToggle } = useDisclosure()

	return (
		<Box>
			<Flex py={3} justifyContent={'center'}>
				<Avatar
					className='Avatar'
					border={'dashed red'}
					w={'40'}
					h={'40'}
					src={img}
					onClick={onToggle}
				/>
			</Flex>
			<Collapse in={isOpen} animateOpacity>
				<Name />
			</Collapse>
		</Box>
	)
}
