import { Box, Center, Heading, Image, Link, Progress } from '@chakra-ui/react'
import Keyboard from '../image/Keyboard.png'
import Mouse from '../image/Mouse.png'
import Headphones from '../image/Headphones.png'
import GraphicTablet from '../image/GraphicTablet.png'
import MousePad from '../image/MousePad.png'

export const Device = () => {
	return (
		<Box
			pt={'2'}
			fontSize='2xl'
			bg={'blackAlpha.400'}
			borderRadius={'20'}
			mt={2}
		>
			<Center>
				<Heading fontSize={'2xl'} fontWeight={'normal'}>
					My Device
				</Heading>
			</Center>
			<Progress
				size='xs'
				mx={'4'}
				mt={'2'}
				colorScheme={'red'}
				isIndeterminate
			/>
			<Center className='Device' m={'4'}>
				<Link
					m={'2'}
					p={'2'}
					border={'outset pink'}
					borderRadius={'20'}
					href={'#'}
				>
					<Image src={Keyboard} m={'auto'} h={'14'} w={'14'} />
					<Center>Keyboard</Center>
				</Link>
				<Link
					m={'2'}
					p={'2'}
					border={'outset pink'}
					borderRadius={'20'}
					href={'#'}
				>
					<Image src={Mouse} m={'auto'} h={'14'} w={'14'} />
					<Center>Mouse</Center>
				</Link>
				<Link
					m={'2'}
					p={'2'}
					border={'outset pink'}
					borderRadius={'20'}
					href={'#'}
				>
					<Image src={MousePad} m={'auto'} h={'14'} w={'14'} />
					<Center>Mat</Center>
				</Link>
				<Link
					m={'2'}
					p={'2'}
					border={'outset pink'}
					borderRadius={'20'}
					href={'#'}
				>
					<Image src={GraphicTablet} m={'auto'} h={'14'} w={'14'} />
					<Center>Tablet</Center>
				</Link>
				<Link
					m={'2'}
					p={'2'}
					border={'outset pink'}
					borderRadius={'20'}
					href={'#'}
				>
					<Image src={Headphones} m={'auto'} h={'14'} w={'14'} />
					<Center>Headphones</Center>
				</Link>
			</Center>
		</Box>
	)
}
