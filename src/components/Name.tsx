import { Badge, Box, Text } from '@chakra-ui/react'

export const Name = () => {
	return (
		<Box
			mb={2}
			p={2}
			rounded={20}
			textAlign={'center'}
			background={'blackAlpha.400'}
			fontSize={'2xl'}
		>
			<Text ml={10} fontWeight='normal'>
				•°¯`•• Z1xonu4a ••´¯°•{' '}
				<Badge colorScheme={'red'} variant={'outline'}>
					Dev
				</Badge>
			</Text>
			<Text fontSize='sm'>Full Stack</Text>
		</Box>
	)
}
