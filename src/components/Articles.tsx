import { Box, Heading, Progress, Text } from '@chakra-ui/react'

export const Articles = () => {
	return (
		<Box p={'4'} borderRadius={'20'} bg={'blackAlpha.400'}>
			<Heading
				fontSize='2xl'
				fontWeight={'normal'}
				display={'flex'}
				justifyContent={'center'}
			>
				About Me
			</Heading>
			<Progress size='xs' mt={'2'} colorScheme={'red'} isIndeterminate />
			<Text mt={'4'} fontSize={'2xl'}>
				Hi, my name is Dima, my nickname is Z1xonu4a. I am 18 years old. I am
				studying web development. I write in TypeScript sometimes for my ass I
				write in JS. Favorite frameworks Chakra UI/Tailwindcss/NextJs. the city
				of Rostov-on-Don. I like to read books at my leisure, I play games very
				rarely because it's not me interesting. I like to watch anime and some
				channels on YouTube or get stuck in a coub. I am currently learning
				TypeScript | React | Chakra UI | tailwindcss."
			</Text>
		</Box>
	)
}
