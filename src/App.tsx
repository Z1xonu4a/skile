import { ChakraProvider, Container } from '@chakra-ui/react'
import { Avatars } from './components/Avatar'
import { Device } from './components/Device'
import { Articles } from './components/Articles'
import { Social } from './components/Social'

export const App = () => (
	<ChakraProvider>
		<Container fontSize={'2xl'}>
			<Avatars />
			<Articles />
			<Device />
			<Social />
		</Container>
	</ChakraProvider>
)
